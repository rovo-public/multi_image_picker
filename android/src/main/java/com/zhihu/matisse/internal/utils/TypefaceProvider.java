package com.zhihu.matisse.internal.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by Joshua on 4/5/15.
 */
public final class TypefaceProvider {
    public static final String TYPEFACE_FOLDER = "fonts";
    public static final String TYPEFACE_EXTENSION = ".ttf";

    public enum Style {
        LIGHT("Light"),
        LIGHT_ITALIC("Light-Italic"),
        REGULAR("Regular"),
        REGULAR_ITALIC("Regular-Italic"),
        MEDIUM("Medium"),
        MEDIUM_ITALIC("Medium-Italic");

        private String name;

        Style(String name) {
            this.name = name;
        }

        public static Style valueOf(int style) {
            switch (style) {
                case 0:
                    return LIGHT;
                case 1:
                    return LIGHT_ITALIC;
                case 2:
                    return REGULAR;
                case 3:
                    return REGULAR_ITALIC;
                case 4:
                    return MEDIUM;
                case 5:
                    return MEDIUM_ITALIC;
                default:
                    return REGULAR;
            }
        }

        public String getName() {
            return name;
        }

        public String getFullName() {
            return "Roboto-" + name;
        }
        }

    private static Hashtable<String, Typeface> sTypeFaces = new Hashtable<String, Typeface>();

    private TypefaceProvider() {
    }

    public static Typeface getTypeFace(Context context, Style style) {
        String fileName = "Roboto-" + style.getName();
        Typeface tempTypeface = sTypeFaces.get(fileName);

        try {
            if (tempTypeface == null) {
                String fontPath = TYPEFACE_FOLDER + '/' + fileName + TYPEFACE_EXTENSION;
                tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
                sTypeFaces.put(fileName, tempTypeface);
            }

            return tempTypeface;
        } catch (Exception e) {
            return Typeface.createFromAsset(context.getAssets(), TYPEFACE_FOLDER + "/Roboto-Regular" + TYPEFACE_EXTENSION);
        }
    }
}
